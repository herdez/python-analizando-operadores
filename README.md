## Operadores en Python

Son símbolos o palabras para realizar una operación específica en variables y/o constantes. Se usan en expresiones como `2 + 5` donde 2 y 5 son operandos y el símbolo `+` es el operador. Son de siete tipos: aritméticos, relacionales, lógicos, de pertenencia, de asignación, de bits y de identidad.


#### Operadores aritméticos

Se usan para realizar operaciones aritméticas: suma, resta, multiplicación, exponente, división y residuo.

```
----------- | --------------------------------------
+               Suma
-               Resta
*               Multiplicación
**              Exponente
/               División
//              División entera
%               Residuo
----------- | --------------------------------------
```

En Python 3, si el resultado de la división entre objetos de tipo entero (int) no es un entero entonces el resultado es un objeto de tipo float.

```
>>> 3 / 2
1.5
>>> 1 / 2
0.5
``` 

##### Precedencia de operadores

La siguiente lista define el orden prioritario de precedencia de los operadores para Python:

```
----------- | --------------------------------------
()              Paréntesis
**              Exponencial
*, /, %, //     Multiplicación, división, módulo y división entera
+, -            Suma y resta
----------- | --------------------------------------
```

Hay que considerar lo siguiente:

- Todas las expresiones entre paréntesis se evalúan primero. 
- Las expresiones con paréntesis anidados se evalúan de adentro hacia afuera, el paréntesis más interno se evalúa primero.
- Los operadores en una misma expresión con igual nivel de prioridad se evalúan de izquierda a derecha.

Algunos ejemplos:

```python
#Exponencial
70 ** 2

#Multiplicación
2 * 5

#División
4 / 2

#Módulo
8 % 4

#Suma
52 + 7

#Resta
23 - 1
```

Una ejemplo de expresión aritmética con varias operaciones:


```
>>> 5 * 5 + 10 / 5 ** 3
25.08
```
En el caso anterior primero se realiza la operación exponencial `5 ** 3` que sería 125, dado que la multiplicación y la división tienen el mismo nivel de prioridad, se realizaría la operación de izquierda a derecha, primero la multiplicación `5 * 5` que es igual a 25 y después la división `10 / 125` que es igual a 0.08. Finalmente se realiza la suma `25 + 0.08` que da como resultado `25.08`.

Es una buena práctica que cuando se programe una expresión aritmética hay que tener en cuenta el manejo de prioridades en cada uno de los operadores matemáticos. Es necesario agrupar por medio de paréntesis cada operación según la fórmula original que se esté usando.

```
>>> (5 * 5) + (10 / (5 ** 3))
25.08
```

Es importante como se usan los paréntesis para no afectar la fórmula original que se esté aplicando:

```
>>> (5 * 5) + (10 / 5) ** 3
33.0
```

#### Operadores relacionales 

Es una comparación entre valores, el resultado de expresiones que usan operadores relacionales es un valor booleano (verdadero o falso / True o False).

```
----------- | --------------------
==          | ¿son iguales?
!=          | ¿son diferentes?
<           | ¿es menor que?
>           | ¿es mayor que? 
<=          | ¿es menor o igual que?
>=          | ¿es mayor o igual que?
----------- | --------------------
```

Algunos ejemplos:

```python
a = 10
b = 10

a1 = 4
b1 = 2
c1 = 2

string1 = "Hello"
string2 = "Bye"

list1 = [2, "Python list", 34]
list2 = [4, "Python list", 45]
```
```
#¿son iguales?
>>> a == b
True
>>> string1 == string2
False
>>> list1 == list2
False

#¿son diferentes?
>>> a1 != b
True
>>> string1 != string2
True

#¿es mayor que?
>>> a1 > b1
True

#¿es menor que?
>>> b1 < a1
True

#¿es mayor o igual que?
>>> b1 >= c1
True

#¿menor o igual que?
>>> b1 <= c1
True

```

#### Operadores lógicos

Se usan para comparar dos expresiones y devolver un resultado booleano (verdadero o falso).

```
----------- | ----------------
and         |   ¿se cumple a y b?
or          |   ¿se cumple a o b?
not         |   No es a
----------- | ----------------
```

##### Operador lógico (and)

```python
True  and True    # => True
True  and False   # => False
False and True    # => False
False and False   # => False
```

##### Operador lógico (or)

```python
True  or True      # => True
True  or False     # => True
False or True      # => True
False or False     # => False
```

##### Operador lógico (not)

```python
not True     # => False
not False    # => True
```

Algunos ejemplos:

```
>>> 20 == 10 or False
False
>>> 20 > 10 and 20 <= 30
True
>>> False and 0
False
>>> not (30 == 30 and not (24 != 24))
False
```

#### Operadores de pertenencia

Los operadores de pertenencia evaluán si un objeto se encuentra dentro de otro. Estos operadores son `in` y `not in`.

Algunos ejemplos:

```
>>> 'auto' in 'automovil'
True
>>> 'hello' in 'hello world'
True
>>> 'Hello' not in 'hello world'
True
```

#### Operadores de asignación

Los operadores de asignación se utilizan para dar un valor a una variable. Existen también operadores de asignación especiales, que son utilizados cuando se desea asignar a una variable su mismo valor aumentado, restado, multiplicado, dividido o potenciado por otro número.

```
----------- | --------------------
=           | x = y
+=          | x = x + y
-=          | x = x - y
*=          | x = x * y
**=         | x = x ** y
/=          | x = x / y
//=         | x = x // y
%=          | x = x % y
----------- | --------------------
```

Algunos ejemplos:

```
>>> x = 10
>>> x += 23
>>> x
33
>>> string = 'auto'
>>> string *= 5 
>>> string
'autoautoautoautoauto'
``` 

#### Operadores de bits

Se realizan sobre cada bit que conforma a un número representado de forma binaria.

```
----------- | --------------------
&           | and
|           | or
^           | xor
~           | not
<<          | desplaza x bits a la izquierda
>>          | desplaza x bits a la derecha
----------- | --------------------
```

```
>>> a = 0b01110
>>> b = 0b10100
>>> b
20
>>> a
14
>>> a & b
4
>>> a | b
30
>>> a ^ b
26
>>> a << 3
112
>>> b >> 4
1
>>> ~ b
-21
```

#### Operadores de identidad

Estos operadores evaluán si una variable se refiere exactamente al mismo objeto o pertenece a un tipo de dato.

```
----------- | --------------------
is          | a is b
is not      | a is not b
----------- | --------------------
```

Algunos ejemplos:

```
>>> string1 = "auto"
>>> string2 = "auto"
>>> number = 8
>>> string1 is string2
True
>>> False is 0
False
>>> False == 0
True
>>> number is int
False
>>> type(number) is int
True
```

## Ejercicio - Analizando operadores en Python

Para este ejercicio hay que documentarse acerca de la estructura condicional `if`, `elif` y `else`, así como el uso de la estructura iterativa `for` en listas.

Analiza el código y `OJO` sin ejecutarlo coloca el resultado en el lugar correspondiente:

```python
operacion1 = (4 < 10 or False) and (True and True)
operacion2 = not (((5 ** 5 == 2 * 2) or not False) and (True and True))
operacion3 = operacion1 == operacion2
operacion3 == False
#>>En esta parte va el resultado de la comparación (True o False)
```

```python
a = [2, 3, 4, 5]
b = [4, 5]
numbers = [value for value in a if value >= 4]
result = numbers == b or False
result == (not False)
#>>En esta parte va el resultado de la comparación (True o False)
```

```python
number = 10
valor = ""

if ((number * 10) / 100 + 0.2) == (24 + 24 - number - 37):
  valor = "Ok"
else:
  valor = "Wrong"

valor == "Ok"
#>>En esta parte va el resultado de la comparación
```

```python
a = 10
b = 5
valor = ""

if a + 10 <= b:
  valor = "Comparación <= | a es Menor o Igual que b!"
elif a + 10 >= b + 16:
  valor = "Comparación >= | a es Mayor o Igual que b!"
elif a + 1 == b + 6:
  valor = "Comparación == | a es IGUAL que b!"
else:
  valor = "NINGUNO!"

valor == "Comparación == | a es IGUAL que b!"
#>>En esta parte va el resultado de la comparación
```